#include <stdio.h>
#include <strings.h>
#include <stdlib.h>

typedef struct employeeData {
    int employeeID;
    char employeeName[20];
    char departmentOfEmployee[20];
    unsigned int birthYear;
    unsigned int salary;
}employeeData;

void menu();
void convertTextFile(FILE* inFilePtr, FILE* outFilePtr);
void addRecord(FILE* fPtr);
void deleteRecord(FILE* fPtr);
void updateRecord(FILE* fPtr);
void showRecords(FILE* fPtr);
void printEmployee(employeeData employee);
int checkEmployeeExistence(int inEmployeeID, FILE* fPtr);

int main() {
    while(1) {
        menu();
    }
    return 0;
}

void menu() {
    FILE *inBinFile, *outTxtFile;
    int choice;

    if ((inBinFile = fopen("employee.bin", "rb+")) == NULL) {
        puts("File employee.bin could not be opened in rb mode.");
        exit(-1);
    }

    if ((outTxtFile = fopen("employee.txt", "w")) == NULL) {
        puts("File employee.txt could not be opened in w mode.");
        return;
    }

    printf("\nEMPLOYEE RECORD SYSTEM\n");
    printf("1 - Add New Record \n2 - Update Record\n");
    printf("3 - Delete Record \n4 - Print All Records\n");
    printf("5 - Save as Text File \n6 - End Program\n\n");

    printf("ENTER A CHOICE: ");
    scanf("%d", &choice);
    switch(choice) {
        case 1:
            addRecord(inBinFile);
            break;
        case 2:
            updateRecord(inBinFile);
            break;
        case 3:
            deleteRecord(inBinFile);
            break;
        case 4:
            showRecords(inBinFile);
            break;
        case 5:
            convertTextFile(inBinFile, outTxtFile);
            break;
        case 6:
            fclose(inBinFile);
            fclose(outTxtFile);
            exit(0);
        default:
            printf("Please choose 1 to 6.\n");
            menu();
    }
}

void addRecord(FILE *fPtr) {
    int inputEmployeeID;
    employeeData employee;

    printf("\nTo exit from add record operation type 0(zero) to Employee ID.\n");
    printf("Enter ID to create new Employee Record: ");
    scanf("%d", &inputEmployeeID);

    if (inputEmployeeID == 0) {
        printf("Terminating Add record operation.\n");
        return;
    }

    if (checkEmployeeExistence(inputEmployeeID, fPtr)) {
        printf("Given Employee ID (#%d) is already taken. Try different ID.\n",
               inputEmployeeID);
        addRecord(fPtr);
        return;
    }

    employee.employeeID = inputEmployeeID;
    printf("Employee Name: ");
    scanf("%s", employee.employeeName);
    printf("Department: ");
    scanf("%s", employee.departmentOfEmployee);
    printf("Birth Year: ");
    scanf("%d", &employee.birthYear);
    printf("Salary: ");
    scanf("%d", &employee.salary);

    fseek(fPtr, (employee.employeeID - 1) * sizeof(employee),
          SEEK_SET);
    fwrite(&employee, sizeof(employee), 1, fPtr);

    printf("Employee with id #%d is recorded.\n", employee.employeeID);
    fclose(fPtr);
    return;
}

void convertTextFile(FILE* inFilePtr, FILE* outFilePtr) {
    employeeData employeeDataTemp[100];
    fprintf(outFilePtr, "%-6s %-18s %-18s %-15s %-11s\n",
           "ID NO", "Employee Name", "Department", "Birth Year", "Salary");
    for (int i=0; i < 100; i++) {
        fread(&employeeDataTemp[i], sizeof(employeeDataTemp[i]), 1, inFilePtr);
        if (employeeDataTemp[i].employeeID != 0) {
            fprintf(outFilePtr, "%-6d %-18s %-18s %-15d %-11d\n", employeeDataTemp[i].employeeID,
                    employeeDataTemp[i].employeeName, employeeDataTemp[i].departmentOfEmployee,
                    employeeDataTemp[i].birthYear, employeeDataTemp[i].salary);
        }
        if(feof(inFilePtr)) {
            break;
        }
    }

    fclose(outFilePtr);
    fclose(inFilePtr);
    printf("employee.txt is ready.");
    return;
}

void deleteRecord(FILE* fPtr) {
    int inputEmployeeID;
    employeeData employee;

    printf("\nTo exit from delete record operation type 0(zero) to Employee ID.\n");
    printf("Enter the Employee ID to delete Record: ");
    scanf("%d", &inputEmployeeID);

    if (inputEmployeeID == 0) {
        printf("Terminating delete record operation.\n");
        return;
    }

    if (!checkEmployeeExistence(inputEmployeeID, fPtr)) {
        printf("Given Employee with ID (#%d) is not exist. Try different ID.\n",
               inputEmployeeID);
        deleteRecord(fPtr);
        return;
    }

    fseek(fPtr, (inputEmployeeID - 1) * sizeof(employee),
          SEEK_SET);
    employee = (employeeData){
        .employeeID = 0,
        .employeeName = "",
        .departmentOfEmployee = "",
        .birthYear = 0,
        .salary = 0
    };

    fwrite(&employee, sizeof(employee), 1, fPtr);

    printf("Employee with id #%d is removed.\n", inputEmployeeID);
    fclose(fPtr);
    return;
}

void updateRecord(FILE* fPtr) {
    int inEmployeeID, percentSalary;
    employeeData tempEmployee;

    printf("\nEnter ID to update salary amount of Employee: ");
    scanf("%d", &inEmployeeID);

    if(!checkEmployeeExistence(inEmployeeID, fPtr)) {
        printf("Given Employee with ID (#%d) is not exist. Try different ID.\n",
               inEmployeeID);
        updateRecord(fPtr);
        return;
    }

    fseek(fPtr, (inEmployeeID - 1) * sizeof(tempEmployee),
          SEEK_SET);
    fread(&tempEmployee, sizeof(tempEmployee), 1, fPtr);
    printf("%-6s %-18s %-18s %-15s %-11s\n",
           "ID NO", "Employee Name", "Department", "Birth Year", "Salary");
    printEmployee(tempEmployee);

    printf("\nEnter the percentage of increase for salary: ");
    scanf("%d", &percentSalary);

    int tempSalary = ((tempEmployee.salary * percentSalary) / 100) + tempEmployee.salary;
    tempEmployee.salary = tempSalary;

    fseek(fPtr, (tempEmployee.employeeID - 1) * sizeof(tempEmployee), SEEK_SET);
    fwrite(&tempEmployee, sizeof(tempEmployee), 1, fPtr);

    printf("%-6s %-18s %-18s %-15s %-11s\n",
           "ID NO", "Employee Name", "Department", "Birth Year", "Salary");
    printEmployee(tempEmployee);

    fclose(fPtr);
    return;
}

void showRecords(FILE* fPtr) {
    employeeData employeeDataTemp[100];

    printf("%-6s %-18s %-18s %-15s %-11s\n",
           "ID NO", "Employee Name", "Department", "Birth Year", "Salary");
    for (int i=0; i < 100; i++) {
        fread(&employeeDataTemp[i], sizeof(employeeDataTemp[i]), 1, fPtr);
        if (employeeDataTemp[i].employeeID != 0) {
            printEmployee(employeeDataTemp[i]);
        }
        if(feof(fPtr)) {
            break;
        }
    }

    fclose(fPtr);
    return;
}

void printEmployee(employeeData employee) {
    printf("%-6d %-18s %-18s %-15d %-11d\n",
           employee.employeeID, employee.employeeName, employee.departmentOfEmployee,
           employee.birthYear, employee.salary);
    return;
}

//check whether given id is exist or not.
//1: record is exist.
//0: record is NOT exist.
int checkEmployeeExistence(int inEmployeeID, FILE* fPtr) {
    employeeData employee;

    fseek(fPtr, (inEmployeeID - 1) * sizeof(employee),
          SEEK_SET);
    fread(&employee, sizeof(employee), 1, fPtr);
    if (employee.employeeID != 0) {
        return 1;
    } else {
        return 0;
    }
}
