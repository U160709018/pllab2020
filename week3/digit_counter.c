#include <stdio.h>

int nrDigits();

int main() {
    int userNum;

    printf("Enter a number: ");
    scanf("%d", &userNum);
    printf("Number of digits: %d\n", nrDigits(userNum));
    return 0;
}

int nrDigits(int num) {
    static int count = 0;

    if (num > 0) {
        count++;
        nrDigits(num/10);
    } else {
        return count;
    }
}
