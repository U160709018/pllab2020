#include <stdio.h>

int findPrime(int number, int divisor);

int main() {
    int userNum;

    printf("Enter a number: ");
    scanf("%d", &userNum);

    int isPrime = findPrime(userNum, userNum/2);
    if (isPrime == 1) {
        printf("%d is a prime number.", userNum);
    }
    else {
        printf("%d is not a prime number.", userNum);
    }
    return 0;
}

//return 1 means that number is prime
//return 0 means that number is not prime
int findPrime(int number, int divisor) {
    if (divisor == 1) {
        return 1;
    }
    else {
        if (number % divisor == 0) {
            return 0;
        }
        else {
            return findPrime(number, divisor - 1);
        }
    }
}


