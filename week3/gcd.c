#include <stdio.h>

int greatestCommonDivisor();

int main() {
    int firstInt, secondInt;
    printf("Enter First integer: ");
    scanf("%d", &firstInt);
    printf("Enter Second integer: ");
    scanf("%d", &secondInt);

    int gcdInt = greatestCommonDivisor(firstInt, secondInt);
    printf("Greatest Common Divisor of %d and %d is %d\n", firstInt, secondInt, gcdInt);

    return 0;
}

int greatestCommonDivisor(int firstInt, int secondInt) {
    if (secondInt == 0) {
        return firstInt;
    } else {
        return greatestCommonDivisor(secondInt, firstInt % secondInt);
    }
}
