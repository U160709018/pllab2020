#include <stdio.h>
#include <math.h>

FILE *filePtr;

int arb_func(float x)
{
    x = x*log10(x) - 1.2;
    return x;
}

int rf(float *x, float x0, float x1, float fx0, float fx1, int *itr)
{
    *x = ((x0*fx1)-(x1*fx0))/(fx1-fx0);
    *itr += 1;
    printf("After %d iterations, root is %d",*itr,x);
    fprintf(outp,"After %d iterations, root is %d\n",*itr,x);
};
int main() {

    int itr, maxitr;
    float x0,x1, x_curr, x_next, error;

    outp = fopen("rf.txt", "w");
    printf("Enter interval values, allowed error and number of iterations:\n");
    scanf("%d, %d,%d, %d", &x0, &x1,error, maxitr);

    rf(x_curr, x0, x1, arb_func(x0), arb_func(x1),itr);
    do {
        if (arb_func(x0) * x_curr<0) {
            x1 = x_curr;
        }
        else {
            x0 = x_curr;
        }

        rf(x_next, x0, x1, arb_func(x0), arb_func(x1),itr);

        if (fabs(x_next-x_curr)<error) {
            for(i=1;i<maxitr+1; i++ ) {
                printf("Iteration %d: %d",i,x_curr);
                fprintf(outp,"Iteration %d: %d\n",i,x_curr);
            }
            return 0;
        } else {
            x_curr = x_curr;
        }
    }
    while (itr < maxitr);
    printf("Solution doesn't converge or iterations are not sufficient");
    fprintf(outp,"Solution doesn't converge or iterations are not sufficient\n");

    fclose(outp);
    return 1;
}
