#include <stdio.h>
#include <string.h>

struct date_t {
    int day;
    int month;
    int year;
};

struct medication {
    char type;
    date_t packaging_date;
    date_t expiration_date;
    int aisle_number;
    char aisle_side;
};

struct skin_hair {
    char type;
    int aisle_number;
    char aisle_side;
};

struct dental {
    date_t packaging_date;
    date_t expiration_date;
};

struct vitamin {
    date_t packaging_date;
    date_t expiration_date;
    int aisle_number;
    char aisle_side;
};

union category_t {
    medication med;
    skin_hair hair;
    dental dent;
    vitamin vit;
};

struct pharmacy_item_t {
    char name[20];
    int unit_cost;
    char product_category;
    category_t category;
};

void print_item(const pharmacy_item_t phar_item);
int loadItems(pharmacy_item_t* phar_items);
int searchItem(const pharmacy_item_t* phar_items, int length, char* query);
int isExpired(const date_t date);

int main() {
    pharmacy_item_t phar_items[10];
    loadItems(phar_items);
}

int loadItems(pharmacy_item_t* phar_items) {
    char file_name[50];
    char line[100];
    const char* contents[12];
    char* token;
    FILE* input_file;
    int line_counter = 0;
    int content_index = 0;

    const char delimiter[] = "\t ";

    printf("Enter the file name to process: ");
    scanf("%s", file_name);

    input_file = fopen(file_name, "r");
    if (input_file == NULL) {
        printf("Couldn't find the given \'%s\' file\n", file_name);
    }

    //get line from the file
    while (fgets(line, 100, input_file) != NULL) {

        token = strtok(line, delimiter);
        while (token != NULL) {

            contents[content_index] = token;
            token = strtok(NULL, delimiter);
        }
        line_counter++;
    }
}

void printItem(const pharmacy_item_t pharmacy_item) {
    printf("**%s**", pharmacy_item.name);
    printf("Unit cost:%d--Type:%c--Packaging:%d/%d/%d--Expiration:%d/%d/%d--Aisle:%d%c\n",
           pharmacy_item.unit_cost, pharmacy_item.product_category);
}
